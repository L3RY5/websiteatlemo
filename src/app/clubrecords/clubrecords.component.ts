import { Component, OnInit,ViewChild,ViewChildren, AfterViewInit, QueryList} from '@angular/core';
import { MatTableDataSource,MatPaginator } from '@angular/material/';

export interface clubRecordsInt {
  discipline: string;
  naam: string;
  datum:string;
  plaats: string;
  records: string;
}

/* begin Tabellen met de clubb records */
/*BEN */
const RECORDBEN_MEISJES: clubRecordsInt[] = [
  {discipline:"60m",naam:"Sieben Yaan",datum:"29-06-13",plaats:"Tienen",records:"9\"87"},
  {discipline:"600m",naam:"Byrne Chinook",datum:"27-06-09",plaats:"Vilvoorde",records:"2'06\"64"},
  {discipline:"1000m",naam:"Byrne Chinook",datum:"03-08-08",plaats:"Lebbeke",records:"4'07\"76"},
  {discipline:"Hoogspringen",naam:"Hasani Geraldine",datum:"18-07-04",plaats:"Dilbeek",records:"1m21"},
  {discipline:"Verspringen",naam:"Hasani Geraldine",datum:"03-07-04",plaats:"Bosvoorde",records:"3m83"},
  {discipline:"kogelstoten",naam:"Hasani Geraldine",datum:"26-06-04",plaats:"Vilvoorde",records:"9m09"},
  {discipline:"hockeybal",naam:"Hasani Geraldine",datum:"27-09-03",plaats:"Nijvel",records:"21m99"},
  {discipline:"4x60m",naam:"",datum:"",plaats:"",records:""},
  {discipline:"vierkamp",naam:"Byrne Chinook",datum:"06-06-09",plaats:"Vilvoorde",records:"1069p"},
 
];
const RECORDBEN_JONGENS: clubRecordsInt[] = [
{discipline:"60m",naam:"Roefmans Lucas",datum:"23-04-10",plaats:"Lebbeke",records:"9\"85"},
{discipline:"600m",naam:"Flachet Liam",datum:"30-05-09",plaats:"Machelen",records:"2'15\"54"},
{discipline:"1000m",naam:"Dari Redouane",datum:"24-05-01",plaats:"St-Niklaas",records:"3'53\"37"},
{discipline:"Hoogspringen",naam:"Philips David",datum:"03-06-12",plaats:"Vilvoorde",records:"1m10"},
{discipline:"Verspringen",naam:"Roefmans Lucas",datum:"03-05-09",plaats:"Anderlecht",records:"3m62"},
{discipline:"kogelstoten",naam:"Ben Sabih Sohaib",datum:"04-07-98",plaats:"Molenbeek",records:"8m01"},
{discipline:"hockeybal",naam:"Amezyan Imadeddine",datum:"20-06-09",plaats:"Molenbeek",records:"27m57"},
{discipline:"4x60m",naam:"",datum:"",plaats:"",records:""},
{discipline:"vierkamp",naam:"Ndabukiye Radjabu Sham",datum:"07-06-14",plaats:"Vilvoorde",records:"1136p"},
];
const RECORDBEN_MEISJESind: clubRecordsInt[] = [
  {discipline:"60m",naam:"Hasani Geraldine",datum:"27-03-04",plaats:"Gent",records:"10\"06"},
  {discipline:"1000m",naam:"Byrne Chinook",datum:"29-12-2007",plaats:"Gent",records:"4'22\"84"},
  {discipline:"Hoogspringen",naam:"",datum:"",plaats:"",records:""},
  {discipline:"Verspringen",naam:"Hasani Geraldine",datum:"10-01-04",plaats:"Woluwe",records:"3m51"},
  {discipline:"kogelstoten",naam:"Hasani Geraldine",datum:"10-01-04",plaats:"Woluwe",records:"7m62"},
  {discipline:"Vierkamp",naam:"",datum:"",plaats:"",records:""},
  
 
];
const RECORDBEN_JONGENSind: clubRecordsInt[] = [
  {discipline:"60m",naam:"Davies Denis",datum:"10-01-04",plaats:"Woluwe",records:"10\"20"},
  {discipline:"1000m",naam:"",datum:"",plaats:"",records:""},
  {discipline:"Hoogspringen",naam:"Davies Denis",datum:"10-01-04",plaats:"Woluwe",records:"85cm"},
  {discipline:"Verspringen",naam:"Zerrad Youne",datum:"23-3-02",plaats:"Gent",records:"2m75"},
  {discipline:"kogelstoten",naam:"Amezyan Imadedine",datum:"13-12-08",plaats:"Woluwe",records:"6m25"},
  {discipline:"Vierkamp",naam:"",datum:"",plaats:"",records:""},

 
];

/*pup*/

const RECORDPUP_MEISJES: clubRecordsInt[] = [
  {discipline:"60m",naam:"Nana-Owusu Philomina",datum:"25-06-11",plaats:"Huizingen",records:"8\"57"},
  {discipline:"1000m",naam:"Byrne Chinook",datum:"10-04-11",plaats:"Ninove",records:"3'30\"76"},
  {discipline:"Hoogspringen",naam:"Ndabukiye Radjabu Asha",datum:"07-06-14",plaats:"Vilvoorde",records:"1m27"},
  {discipline:"Verspringen",naam:"Nana-Owusu Philomina",datum:"02-07-11",plaats:"Machelen",records:"3m86"},
  {discipline:"kogelstoten",naam:"Hassani Geraldina",datum:"25-06-05",plaats:"Dilbeek",records:"8m89"},
  {discipline:"hockeybal",naam:"Byrne Chinook",datum:"02-07-11",plaats:"Machelen",records:"27m82"},
  {discipline:"Discuswerpen",naam:"Ackerman Aurelie",datum:"02-09-00",plaats:"Vilvoorde",records:"18m11"},
  {discipline:"4x60m",naam:"Wester Yasmine, Lamsadya Manel,Nicaise Eline,Byrne Chinook",datum:"17-04-10",plaats:"Dilbeek",records:"42\"58"},
  {discipline:"Vierkamp",naam:"Nana-Owusu Philomina",datum:"02-07-11",plaats:"Machelen",records:"1776p"},
 
 
];
const RECORDPUP_JONGENS: clubRecordsInt[] = [
  {discipline:"60m",naam:"Roefmans Lucas",datum:"23-06-12",plaats:"Dilbeek",records:"8\"87"},
  {discipline:"1000m",naam:"Kamil Yassin",datum:"12-06-04",plaats:"Vilvoorde",records:"3'33\"21"},
  {discipline:"60Horden",naam:"",datum:"",plaats:"",records:""},
  {discipline:"Hoogspringen",naam:"Davies Isaac",datum:"28-06-03",plaats:"Huizingen",records:"1m20"},
  {discipline:"Verspringen",naam:"Akhedov Manzul",datum:"28-08-05",plaats:"Anderlecht",records:"3m90"},
  {discipline:"kogelstoten",naam:"Karsikaya Murat",datum:"06-06-98",plaats:"Machelen",records:"7m50"},
  {discipline:"hockeybal",naam:"Melhaoui Anouar",datum:"19-08-01",plaats:"Dilbeek",records:"33m08"},
  {discipline:"Discuswerpen",naam:"V. Outenboer Karim",datum:"17-04-99",plaats:"Brussel",records:"13m35"},
  {discipline:"4x60m",naam:"Roefmans Lucas,Jacques Meert Andries, Ayad Hamza, Van Hecke Honor",datum:"22-04-12",plaats:"Huizingen",records:"40\"09"},
  {discipline:"Vierkamp",naam:"Akhmedov Manzul",datum:"28-08-05",plaats:"Anderlecht",records:"1691p"},
 
];

const RECORDPUP_MEISJESind: clubRecordsInt[] = [
  {discipline:"60m",naam:"Byrne Chinook",datum:"26-12-09",plaats:"Gent",records:"10\"16"},
  {discipline:"1000m",naam:"",datum:"",plaats:"",records:""},
  {discipline:"Hoogspringen",naam:"",datum:"",plaats:"",records:""},
  {discipline:"Verspringen",naam:"Byrne Chinook",datum:"9-12-07",plaats:"Gent",records:"2m65"},
  {discipline:"kogelstoten",naam:"Dari Jamaa",datum:"30-12-00",plaats:"Gent",records:"2m73"},
  {discipline:"Vierkamp",naam:"",datum:"",plaats:"",records:""},
];
const RECORDPUP_JONGENSind: clubRecordsInt[] = [
  {discipline:"60m",naam:"Dannou Mohamed",datum:"15-12-01",plaats:"Woluwe",records:"10\"10"},
  {discipline:"1000m",naam:"Yahklef Hamza",datum:"23-02-02",plaats:"Gent",records:"3'49\"33"},
  {discipline:"60m Horden",naam:"",datum:"",plaats:"",records:""},
  {discipline:"Hoogspringen",naam:"Yahklef Hamza",datum:"1-12-01",plaats:"Woluwe",records:"1m05"},
  {discipline:"Verspringen",naam:"Dahhou Mohamed	",datum:"1-12-01",plaats:"Woluwe",records:"3m65"},
  {discipline:"kogelstoten",naam:"Bougdim Amine	",datum:"14-1-01",plaats:"Gent",records:"5m65"},
  {discipline:"Vierkamp",naam:"",datum:"",plaats:"",records:""},
];

/*Min */

const RECORDMIN_MEISJES: clubRecordsInt[] = [
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
 
];
const RECORDMIN_JONGENS: clubRecordsInt[] = [
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
 
];

const RECORDMIN_MEISJESind: clubRecordsInt[] = [
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
 
];
const RECORDMIN_JONGENSind: clubRecordsInt[] = [
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
 
];

/*KAD */

const RECORDKAD_DAMES: clubRecordsInt[] = [
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
 
];
const RECORDKAD_HEREN: clubRecordsInt[] = [
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
 
];

const RECORDKAD_DAMESind: clubRecordsInt[] = [
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
 
];
const RECORDKAD_HERENind: clubRecordsInt[] = [
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
 
];

/*SCH */

const RECORDSCH_DAMES: clubRecordsInt[] = [
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
 
];
const RECORDSCH_HEREN: clubRecordsInt[] = [
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
 
];

const RECORDSCH_DAMESind: clubRecordsInt[] = [
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
 
];
const RECORDSCH_HERENind: clubRecordsInt[] = [
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
 
];

/*Jun */

const RECORDJUN_DAMES: clubRecordsInt[] = [
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
 
];
const RECORDJUN_HEREN: clubRecordsInt[] = [
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
 
];

const RECORDJUN_DAMESind: clubRecordsInt[] = [
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
 
];
const RECORDJUN_HERENind: clubRecordsInt[] = [
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
 
];
/*SEN */

const RECORDSEN_DAMES: clubRecordsInt[] = [
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
 
];
const RECORDSEN_HEREN: clubRecordsInt[] = [
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
 
];

const RECORDSEN_DAMESind: clubRecordsInt[] = [
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
 
];
const RECORDSEN_HERENind: clubRecordsInt[] = [
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
 
];

/*MAS */

const RECORDMAS_DAMES: clubRecordsInt[] = [
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
 
];
const RECORDMAS_HEREN: clubRecordsInt[] = [
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
 
];

const RECORDMAS_DAMESind: clubRecordsInt[] = [
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
 
];
const RECORDMAS_HERENind: clubRecordsInt[] = [
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
  {discipline:"",naam:"",datum:"",plaats:"",records:""},
 
];

/* einde Tabellen met de clubb records */
@Component({
  selector: 'app-clubrecords',
  templateUrl: './clubrecords.component.html',
  styleUrls: ['./clubrecords.component.css']
})
export class ClubrecordsComponent implements OnInit  {
  /**outdoor */
  dataSourceBenM : MatTableDataSource<clubRecordsInt>;
  dataSourceBenJ : MatTableDataSource<clubRecordsInt>;
  dataSourcePupM : MatTableDataSource<clubRecordsInt>;
  dataSourcePupJ : MatTableDataSource<clubRecordsInt>;
  dataSourceMinM : MatTableDataSource<clubRecordsInt>;
  dataSourceMinJ : MatTableDataSource<clubRecordsInt>;
  dataSourceKadD : MatTableDataSource<clubRecordsInt>;
  dataSourceKadH : MatTableDataSource<clubRecordsInt>;
  dataSourceSchD : MatTableDataSource<clubRecordsInt>;
  dataSourceSchH : MatTableDataSource<clubRecordsInt>;
  dataSourceJunD : MatTableDataSource<clubRecordsInt>;
  dataSourceJunH : MatTableDataSource<clubRecordsInt>;
  dataSourceSenD : MatTableDataSource<clubRecordsInt>;
  dataSourceSenH : MatTableDataSource<clubRecordsInt>;
  dataSourceMasD : MatTableDataSource<clubRecordsInt>;
  dataSourceMasH : MatTableDataSource<clubRecordsInt>;

  /**indoor */
  dataSourceBenMind : MatTableDataSource<clubRecordsInt>;
  dataSourceBenJind : MatTableDataSource<clubRecordsInt>;
  dataSourcePupMind : MatTableDataSource<clubRecordsInt>;
  dataSourcePupJind : MatTableDataSource<clubRecordsInt>;
  dataSourceMinMind : MatTableDataSource<clubRecordsInt>;
  dataSourceMinJind : MatTableDataSource<clubRecordsInt>;
  dataSourceKadDind : MatTableDataSource<clubRecordsInt>;
  dataSourceKadHind : MatTableDataSource<clubRecordsInt>;
  dataSourceSchDind : MatTableDataSource<clubRecordsInt>;
  dataSourceSchHind : MatTableDataSource<clubRecordsInt>;
  dataSourceJunDind : MatTableDataSource<clubRecordsInt>;
  dataSourceJunHind : MatTableDataSource<clubRecordsInt>;
  dataSourceSenDind : MatTableDataSource<clubRecordsInt>;
  dataSourceSenHind : MatTableDataSource<clubRecordsInt>;
  dataSourceMasDind : MatTableDataSource<clubRecordsInt>;
  dataSourceMasHind : MatTableDataSource<clubRecordsInt>;



  @ViewChildren(MatPaginator) paginator = new QueryList<MatPaginator>();
  @ViewChildren(MatPaginator) paginatorIndoor = new QueryList<MatPaginator>();
  

  displayedColumns: string[] = ['discipline', 'naam','datum', 'plaats','records'];

 
  



  panelOpenState = false;

  constructor() { 
     /*Datasources tabellen outdoor */
    this.dataSourceBenM =  new MatTableDataSource<clubRecordsInt>(RECORDBEN_MEISJES);
    this.dataSourceBenJ =  new MatTableDataSource<clubRecordsInt>(RECORDBEN_JONGENS);
    this.dataSourcePupM =  new MatTableDataSource<clubRecordsInt>(RECORDPUP_MEISJES);
    this.dataSourcePupJ =  new MatTableDataSource<clubRecordsInt>(RECORDPUP_JONGENS);
    this.dataSourceMinM =  new MatTableDataSource<clubRecordsInt>(RECORDMIN_MEISJES);
    this.dataSourceMinJ =  new MatTableDataSource<clubRecordsInt>(RECORDMIN_JONGENS);
    this.dataSourceKadD =  new MatTableDataSource<clubRecordsInt>(RECORDKAD_DAMES);
    this.dataSourceKadH =  new MatTableDataSource<clubRecordsInt>(RECORDKAD_HEREN);
    this.dataSourceSchD =  new MatTableDataSource<clubRecordsInt>(RECORDSCH_DAMES);
    this.dataSourceSchH =  new MatTableDataSource<clubRecordsInt>(RECORDSCH_HEREN);
    this.dataSourceJunD =  new MatTableDataSource<clubRecordsInt>(RECORDJUN_DAMES);
    this.dataSourceJunH =  new MatTableDataSource<clubRecordsInt>(RECORDJUN_HEREN);
    this.dataSourceSenD =  new MatTableDataSource<clubRecordsInt>(RECORDSEN_DAMES);
    this.dataSourceSenH =  new MatTableDataSource<clubRecordsInt>(RECORDSEN_HEREN);
    this.dataSourceMasD =  new MatTableDataSource<clubRecordsInt>(RECORDMAS_DAMES);
    this.dataSourceMasH =  new MatTableDataSource<clubRecordsInt>(RECORDMAS_HEREN);


  /*einde datasources tabellen */

    /*Datasources tabellen indoor */
    this.dataSourceBenMind =  new MatTableDataSource<clubRecordsInt>(RECORDBEN_MEISJESind);
    this.dataSourceBenJind =  new MatTableDataSource<clubRecordsInt>(RECORDBEN_JONGENSind);
    this.dataSourcePupMind =  new MatTableDataSource<clubRecordsInt>(RECORDPUP_MEISJESind);
    this.dataSourcePupJind =  new MatTableDataSource<clubRecordsInt>(RECORDPUP_JONGENSind);
    this.dataSourceMinMind =  new MatTableDataSource<clubRecordsInt>(RECORDMIN_MEISJESind);
    this.dataSourceMinJind =  new MatTableDataSource<clubRecordsInt>(RECORDMIN_JONGENSind);
    this.dataSourceKadDind =  new MatTableDataSource<clubRecordsInt>(RECORDKAD_DAMESind);
    this.dataSourceKadHind =  new MatTableDataSource<clubRecordsInt>(RECORDKAD_HERENind);
    this.dataSourceSchDind =  new MatTableDataSource<clubRecordsInt>(RECORDSCH_DAMESind);
    this.dataSourceSchHind =  new MatTableDataSource<clubRecordsInt>(RECORDSCH_HERENind);
    this.dataSourceJunDind =  new MatTableDataSource<clubRecordsInt>(RECORDJUN_DAMESind);
    this.dataSourceJunHind =  new MatTableDataSource<clubRecordsInt>(RECORDJUN_HERENind);
    this.dataSourceSenDind =  new MatTableDataSource<clubRecordsInt>(RECORDSEN_DAMESind);
    this.dataSourceSenHind =  new MatTableDataSource<clubRecordsInt>(RECORDSEN_HERENind);
    this.dataSourceMasDind =  new MatTableDataSource<clubRecordsInt>(RECORDMAS_DAMESind);
    this.dataSourceMasHind =  new MatTableDataSource<clubRecordsInt>(RECORDMAS_HERENind);

     /*einde datasources tabellen indoor */
  }

  ngOnInit() {
   
  }
  ngAfterViewInit() {
/*outdoor*/
    this.dataSourceBenM.paginator=this.paginator.toArray()[0];
    this.dataSourceBenJ.paginator=this.paginator.toArray()[1];
    this.dataSourcePupM.paginator=this.paginator.toArray()[2];
    this.dataSourcePupJ.paginator=this.paginator.toArray()[3];
    this.dataSourceMinM.paginator=this.paginator.toArray()[4];
    this.dataSourceMinJ.paginator=this.paginator.toArray()[5];
    this.dataSourceKadD.paginator=this.paginator.toArray()[6];
    this.dataSourceKadH.paginator=this.paginator.toArray()[7];
    this.dataSourceSchD.paginator=this.paginator.toArray()[8];
    this.dataSourceSchH.paginator=this.paginator.toArray()[9];
    this.dataSourceJunD.paginator=this.paginator.toArray()[10];
    this.dataSourceJunH.paginator=this.paginator.toArray()[11];
    this.dataSourceSenD.paginator=this.paginator.toArray()[12];
    this.dataSourceSenH.paginator=this.paginator.toArray()[13];
    this.dataSourceMasD.paginator=this.paginator.toArray()[14];
    this.dataSourceSenH.paginator=this.paginator.toArray()[15];


    /**Indoor */
    this.dataSourceBenMind.paginator=this.paginator.toArray()[16];
    this.dataSourceBenJind.paginator=this.paginator.toArray()[17];
    this.dataSourcePupMind.paginator=this.paginator.toArray()[18];
    this.dataSourcePupJind.paginator=this.paginator.toArray()[19];
    this.dataSourceMinMind.paginator=this.paginator.toArray()[20];
    this.dataSourceMinJind.paginator=this.paginator.toArray()[21];
    this.dataSourceKadDind.paginator=this.paginator.toArray()[22];
    this.dataSourceKadHind.paginator=this.paginator.toArray()[23];
    this.dataSourceSchDind.paginator=this.paginator.toArray()[24];
    this.dataSourceSchHind.paginator=this.paginator.toArray()[25];
    this.dataSourceJunDind.paginator=this.paginator.toArray()[26];
    this.dataSourceJunHind.paginator=this.paginator.toArray()[27];
    this.dataSourceSenDind.paginator=this.paginator.toArray()[28];
    this.dataSourceSenHind.paginator=this.paginator.toArray()[29];
    this.dataSourceMasDind.paginator=this.paginator.toArray()[30];
    this.dataSourceMasHind.paginator=this.paginator.toArray()[31];
  }


}
