import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  
})

export class HeaderComponent implements OnInit {
  //images = [1, 2, 3].map(() => `https://picsum.photos/900/500?random&t=${Math.random()}`);
  //images = [1, 2, 3].map(() => '../../assets/img/header?random&t=${Math.random()}');
  images = ["../../assets/img/header1.jpg", "../../assets/img/header2.jpg", "../../assets/img/header4.jpg"];
  constructor() { }

  ngOnInit() {
  }

}
