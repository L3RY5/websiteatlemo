import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-algemene-informatie',
  templateUrl: './algemene-informatie.component.html',
  styleUrls: ['./algemene-informatie.component.css']
})
export class AlgemeneInformatieComponent implements OnInit {
isCollapsed : boolean=true;
  constructor() { }

  meerMinderToggle(){this.isCollapsed = !this.isCollapsed;}
  ngOnInit() {
  }

}
