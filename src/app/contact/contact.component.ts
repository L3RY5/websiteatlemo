import { Component, OnInit, NgModule } from '@angular/core';
import {ErrorStateMatcher} from '@angular/material/core';
import {FormBuilder,FormGroup,FormControl,FormGroupDirective,NgForm,Validators}from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';


/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || isSubmitted));
  }
}

export interface Onderwerp {
  value: string;
  viewValue: string;
}

/** Error when invalid control is dirty, touched, or submitted. */
/* export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
} */
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})


export class ContactComponent implements OnInit {
  selectedOnderwerp: string;
  naamFormControl = new FormControl('', [Validators.required,
    this.noWhitespaceValidator,
  
  ]);
  voorNaamFormControl = new FormControl('', [Validators.required,
    this.noWhitespaceValidator,
   
  ]);
comentaarFormControl = new FormControl('', [Validators.required,
    this.noWhitespaceValidator,
  ]);
 telFormControl = new FormControl('', [Validators.required,
    this.noWhitespaceValidator,this.MinTenLetters


  ]);
  onderwerpFormControl = new FormControl('', [Validators.required,
    this.noWhitespaceValidator,


  ]);
  emailFormControl = new FormControl('', [Validators.required,
    this.noWhitespaceValidator,Validators.email


  ]);

  matcher = new MyErrorStateMatcher();
  constructor(private _snackBar: MatSnackBar) { }

  ngOnInit() {
  }
  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;

    return isValid ? null : {
      'whitespace': true
    }

    ;
  }

  public MinTenLetters(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length < 10;
    const isValid = !isWhitespace;
    return isValid ? null : {
      'smallerdanTen': true
    }

    ;
  }

  sendContactMail(){
    if (this.naamFormControl.valid&&this.voorNaamFormControl.valid&&
        this.emailFormControl.valid&&this.telFormControl.valid&&
        this.comentaarFormControl.valid&&this.onderwerpFormControl.valid) {
      console.log("ok");
      
    } else {
      this._snackBar.open("1 of meerdere velden zijn niet correct ingevuld", "Sluiten", {
        duration: 5000,
      });
    }
  }

}
