import { Component, OnInit } from '@angular/core';

export interface TrainingsTijden {
  dag: string;
  tijd: string;
  categorie: string;
 
}

const PisteTraining: TrainingsTijden[] = [
  {dag: "dinsdag & Vrijdag", tijd: '17:15-18:30', categorie: "Jeugd tot Min "},
  {dag: "Donderdag", tijd: '17:15-18:30', categorie: " Min "},
  {dag: "dinsdag, Donderdag, Vrijdag", tijd: '17:15-19:00', categorie: " Kad en schol "},
  {dag: "dinsdag & Vrijdag", tijd: '17:30-18:30', categorie: " Recreatengroep "},
  {dag: "dinsdag, Donderdag & Vrijdag", tijd: '17:30-19:00', categorie: "Sprinters en Langeafstandsgroep"},
  
];
const KrachtTraining: TrainingsTijden[] = [
  {dag: "Maandag & Woensdad", tijd: '17u30 tot 19u00 ', categorie: "+14 Jaar"},
  {dag: "Maandag & Woensdad", tijd: '17u30 tot 19u00 ', categorie: "+14 Jaar"},
  
  
];
@Component({
  selector: 'app-trainingen',
  templateUrl: './trainingen.component.html',
  styleUrls: ['./trainingen.component.css']
})
export class TrainingenComponent implements OnInit {
  displayedColumns: string[] = ['dag', 'tijd', 'categorie'];
  dataSourcePiste = PisteTraining;
  dataSourceKracht = KrachtTraining;
  panelOpenState = false;
  constructor() { }

  ngOnInit() {
  }

}
