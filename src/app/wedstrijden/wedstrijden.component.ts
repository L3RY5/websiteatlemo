import { Component, OnInit, ViewChild,ViewChildren, AfterViewInit, QueryList } from '@angular/core';
import { MatTableDataSource,MatPaginator } from '@angular/material/';
import {ErrorStateMatcher}from '@angular/material/core';
import {MAT_MOMENT_DATE_FORMATS,MomentDateAdapter}from '@angular/material-moment-adapter';
import {DateAdapter,MAT_DATE_FORMATS,MAT_DATE_LOCALE}from '@angular/material/core';
import {FormBuilder,FormGroup,FormControl,FormGroupDirective,NgForm,Validators}from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || isSubmitted));
  }
}

export interface VeldLoopJongeren {
  datum: string;
  adres: string;
  club:string;
  eersteStart: string;
  samenKomst: string;
 
}
export interface wedstrijdZomer {
  wedstrindINfo: string;
  samenkomst: string;
  benPup:string;
  min:string;
  kadSchol: string;
  jun: string;
  sen: string;
  mas: string;
}

const WEDSTRRIID_WINTER: VeldLoopJongeren[] = [
  {datum: '28/11/2014',club:"naam - naamdomain", adres: '17:15-18:30', eersteStart: "12:30",samenKomst:"11u00 bij Atlemo"},
  {datum: '20/12/2015', club:"naam - naamdomain",adres: '17:15-18:30', eersteStart: "12:30",samenKomst:"11u00 bij Atlemo"},
  {datum: '07/02/2016 ',club:"naam - naamdomain", adres: '17:15-18:30', eersteStart:"12:30",samenKomst:"11u00 bij Atlemo"},
  {datum: '05/03/2016', club:"atlemo - stippelberg",adres: 'Mahatma Gandhilaan 5 - 1080 Sint-Jans-Molenbeek ', eersteStart: "12:30",samenKomst:"11u00 bij Atlemo"},
  {datum: '05/04/2016', club:"VAC - Park 3 fonteinen",adres: 'Beneluxlaan - 1800 Vilvoorde', eersteStart: "12:30",samenKomst:"11u00 bij Atlemo"},
  {datum: '23/01/2016', club:"OAC - Hof ten Hemelrijk",adres: 'Kloosterstraat 7 - opwijk ', eersteStart: "alle Leefijden ",samenKomst:""},
  {datum: '28/11/2014',club:"naam - naamdomain", adres: '17:15-18:30', eersteStart: "12:30",samenKomst:"11u00 bij Atlemo"},
  {datum: '20/12/2015', club:"naam - naamdomain",adres: '17:15-18:30', eersteStart: "12:30",samenKomst:"11u00 bij Atlemo"},
  {datum: '07/02/2016 ',club:"naam - naamdomain", adres: '17:15-18:30', eersteStart:"12:30",samenKomst:"11u00 bij Atlemo"},
  {datum: '05/03/2016', club:"atlemo - stippelberg",adres: 'Mahatma Gandhilaan 5 - 1080 Sint-Jans-Molenbeek ', eersteStart: "12:30",samenKomst:"11u00 bij Atlemo"},
  {datum: '05/04/2016', club:"VAC - Park 3 fonteinen",adres: 'Beneluxlaan - 1800 Vilvoorde', eersteStart: "12:30",samenKomst:"11u00 bij Atlemo"},
  {datum: '23/01/2016', club:"OAC - Hof ten Hemelrijk",adres: 'Kloosterstraat 7 - opwijk ', eersteStart: "alle Leefijden ",samenKomst:""},
  
];

const WEDSTRRIID_ZOMER: wedstrijdZomer[] = [
{wedstrindINfo:"07/05/2016\nZaterdag 12u00\n(MAC) Derooverstadion\nStadionstraat 1830 Machelen",samenkomst:"10u00 F. Vandesandestr 1081 Koekelberg",benPup:"4Kamp",min:"5Kamp",kadSchol:"5Kamp\n100m,200m\n800m,1500m",jun:"5Kamp\n100m,200m\n800m,1500m",sen:"5Kamp\n100m,200m\n800m,1500m,5000m",mas:"5Kamp\n100m,200m\n800m,1500m"},
{wedstrindINfo:"07/05/2016\nZaterdag 12u00\n(MAC) Derooverstadion\nStadionstraat 1830 Machelen",samenkomst:"10u00 F. Vandesandestr 1081 Koekelberg",benPup:"4Kamp",min:"5Kamp",kadSchol:"5Kamp\n100m,200m\n800m,1500m",jun:"5Kamp\n100m,200m\n800m,1500m",sen:"5Kamp\n100m,200m\n800m,1500m,5000m",mas:"5Kamp\n100m,200m\n800m,1500m"},
{wedstrindINfo:"07/05/2016\nZaterdag 12u00\n(MAC) Derooverstadion\nStadionstraat 1830 Machelen",samenkomst:"10u00 F. Vandesandestr 1081 Koekelberg",benPup:"4Kamp",min:"5Kamp",kadSchol:"5Kamp\n100m,200m\n800m,1500m",jun:"5Kamp\n100m,200m\n800m,1500m",sen:"5Kamp\n100m,200m\n800m,1500m,5000m",mas:"5Kamp\n100m,200m\n800m,1500m"},
{wedstrindINfo:"07/05/2016\nZaterdag 12u00\n(MAC) Derooverstadion\nStadionstraat 1830 Machelen",samenkomst:"10u00 F. Vandesandestr 1081 Koekelberg",benPup:"4Kamp",min:"5Kamp",kadSchol:"5Kamp\n100m,200m\n800m,1500m",jun:"5Kamp\n100m,200m\n800m,1500m",sen:"5Kamp\n100m,200m\n800m,1500m,5000m",mas:"5Kamp\n100m,200m\n800m,1500m"},
{wedstrindINfo:"07/05/2016\nZaterdag 12u00\n(MAC) Derooverstadion\nStadionstraat 1830 Machelen",samenkomst:"10u00 F. Vandesandestr 1081 Koekelberg",benPup:"4Kamp",min:"5Kamp",kadSchol:"5Kamp\n100m,200m\n800m,1500m",jun:"5Kamp\n100m,200m\n800m,1500m",sen:"5Kamp\n100m,200m\n800m,1500m,5000m",mas:"5Kamp\n100m,200m\n800m,1500m"},
{wedstrindINfo:"07/05/2016\nZaterdag 12u00\n(MAC) Derooverstadion\nStadionstraat 1830 Machelen",samenkomst:"10u00 F. Vandesandestr 1081 Koekelberg",benPup:"4Kamp",min:"5Kamp",kadSchol:"5Kamp\n100m,200m\n800m,1500m",jun:"5Kamp\n100m,200m\n800m,1500m",sen:"5Kamp\n100m,200m\n800m,1500m,5000m",mas:"5Kamp\n100m,200m\n800m,1500m"},
{wedstrindINfo:"07/05/2016\nZaterdag 12u00\n(MAC) Derooverstadion\nStadionstraat 1830 Machelen",samenkomst:"10u00 F. Vandesandestr 1081 Koekelberg",benPup:"4Kamp",min:"5Kamp",kadSchol:"5Kamp\n100m,200m\n800m,1500m",jun:"5Kamp\n100m,200m\n800m,1500m",sen:"5Kamp\n100m,200m\n800m,1500m,5000m",mas:"5Kamp\n100m,200m\n800m,1500m"},
];

@Component({
  selector: 'app-wedstrijden',
  templateUrl: './wedstrijden.component.html',
  styleUrls: ['./wedstrijden.component.css'],
  providers: [ // The locale would typically be provided on the root module of your application. We do it at

    // the component level here, due to limitations of our example generation script.
    {
      provide: MAT_DATE_LOCALE,
      useValue: 'nl-NL'
    }

    ,

    // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
    // `MatMomentDateModule` in your applications root module. We provide it at the component level
    // here, due to limitations of our example generation script.
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    }

    ,
    {
      provide: MAT_DATE_FORMATS,
      useValue: MAT_MOMENT_DATE_FORMATS
    }

    ,
  ],
})


export class WedstrijdenComponent implements OnInit {
  firstFormGroup: FormGroup;
  dataSourceWinter :MatTableDataSource<VeldLoopJongeren>;
  dataSourceZomer :MatTableDataSource<wedstrijdZomer>;

  @ViewChildren(MatPaginator) paginator = new QueryList<MatPaginator>();
  displayedColumns: string[] = ['datum', 'club','adres', 'eersteStart','samenKomst'];
  displayedZomer: string[] = ['wedstrindINfo', 'samenkomst','benPup', 'min','kadSchol','jun','sen','mas'];
 

  naamFormControl = new FormControl('', [Validators.required,
    this.noWhitespaceValidator,
  
  ]);
  voorNaamFormControl = new FormControl('', [Validators.required,
    this.noWhitespaceValidator,
   
  ]);
  geboorteJaarControl = new FormControl('', [Validators.required,
    this.noWhitespaceValidator,this.MinFourLetters
  ]);
  locatieFormControl = new FormControl('', [Validators.required,
    this.noWhitespaceValidator,


  ]);
  datumWedFormControl = new FormControl('', [Validators.required,
    this.noWhitespaceValidator,


  ]);
  diciplineFormControl = new FormControl('', [Validators.required,
    this.noWhitespaceValidator,


  ]);


  

  matcher = new MyErrorStateMatcher();
  constructor(private _adapter: DateAdapter < any > ,private _snackBar: MatSnackBar) {
    this.dataSourceWinter=new MatTableDataSource<VeldLoopJongeren>(WEDSTRRIID_WINTER);
    this.dataSourceZomer=new MatTableDataSource<wedstrijdZomer>(WEDSTRRIID_ZOMER);
   }

ngAfterViewInit() {
  this.dataSourceWinter.paginator=this.paginator.toArray()[0];
  this.dataSourceZomer.paginator=this.paginator.toArray()[1];
}
  ngOnInit() {

  }
  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;

    return isValid ? null : {
      'whitespace': true
    }

    ;
  }

  public MinFourLetters(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length < 4;
    const isValid = !isWhitespace;
    return isValid ? null : {
      'smallerdanfor': true
    }

    ;
  }

applyFilter(filterValue:string){
  this.dataSourceWinter.filter = filterValue.trim();
 
}
applyFilterZomer(filterValue:string){
  this.dataSourceZomer.filter = filterValue.trim();
 
}

wedstrijdInschrijving(){
  if (this.naamFormControl.valid && this.voorNaamFormControl.valid&&
      this.geboorteJaarControl.valid && this.datumWedFormControl.valid&&
      this.diciplineFormControl.valid && this.locatieFormControl.valid) {
        console.log("ok send mail");
  } else {
    this._snackBar.open("1 of meerdere velden zijn niet correct ingevuld", "Sluiten", {
      duration: 5000,
    });
  }
}

}
