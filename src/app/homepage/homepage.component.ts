import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
isCollapsed : boolean=true;
  constructor() { }

  meerMinderToggle(){this.isCollapsed = !this.isCollapsed;}
  ngOnInit() {
  }

}
